#!/usr/bin/env python3

from datetime import datetime
import sh
from smtplib import SMTP_SSL

# Checks for updates with 'softwareupdate --list' and
# alerts admin by email if it detects them

# SETUP ADDRESSES
_SendTo = ""
_SendFrom = ""

# SETUP MAILSERVER LOGIN
_MailserverAndPort = "SERVER:PORT"
_MailserverUser = ""
_MailserverPassword = ""

def check_if_updates(output):
    output = str(output)
    if output.split('\n')[4].strip() == '':
        return False
    else:
        return '\n'.join(output.split('\n')[4:])

def send_alert(updates):
    HOST = _MailserverAndPort
    username = _MailserverUser
    password = _MailserverPassword

    TO = 'To: {}'.format(_SendTo)
    FROM = 'From: {}'.format(_SendFrom)
    SUBJECT = "Subject: Updates available on {}".format(sh.hostname())
    msg = updates.strip()

    BODY = "{}\n{}\n{}\n\n{}".format(TO,FROM,SUBJECT,msg)

    try:
        server = SMTP_SSL(HOST)
        server.login(username, password)
        server.sendmail(FROM, TO, BODY)
    except:
        raise
    finally:
        server.quit()


if __name__ == '__main__':
    updates = check_if_updates(sh.softwareupdate("--list"))
    if updates:
        send_alert(updates)
