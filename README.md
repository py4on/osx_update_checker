# OSX Update Checker
## (a simple, apticron-ish solution for macs)

## Description
For those of us who use mac minis as servers (TimeMachine network backups etc) it's easy to forget about software updates. By scheduling this script to run everyday via cron (see below) you'll be notified by email as soon as Apple Software Updates (security patches etc) are available.

To install them, issue `softwareupdate --install --all` (see the [Apple Developer Library](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/softwareupdate.8.html) for more options). 

### Requirements

*Install Python requirements using `pip install <requirement>`. If you don't have `pip` [it only takes a second](https://pip.pypa.io/en/latest/installing/).*<br /> 
*If you're using `virtualenv` this will have been setup already.*

1. [**`sh`**](https://github.com/amoffat/sh) (a really clean way of calling system programs as Python functions)

2. You're also going to need an email account to send updates from (Gmail is fine but any old POP/IMAP account will do)


### Setup / Installation
1. Open `update_checker.py` in a text editor and fill in the setup sections at the top. This is pretty self-explanatory but make sure you use a colon to separate your mailserver and port (as in the example string) i.e. `example.com:465`.
2. Move the script into place:

        mv update_check.py /usr/local/bin/update_check

3. Schedule it to run daily by editing crontab with: 

        crontab -e 
(*DO NOT* use `sudo` for this as crontab is user-specific)

4. Enter/paste the following for daily update-checking (a la apticron). Obviously, the path for your python3 may well be different to the below (use `which python3` to get your path). If your preferred python3 installation is inside a virtualenv, insert the full path to it (e.g. `/Users/bob/envs/py3/bin/python`):

        0 12 * * * /usr/local/bin/python3 /usr/local/bin/update_check

5. Save your changes, close your editor, and you're done!


<br />
#### `cron`: some potential pitfalls
If you're not alreay acquainted with `cron`, please bear the following in mind:
* Always make sure there is a `PATH` variable at the top of the file:

        PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

* Ensure you have included a blank line at the end of your crontab instructions.

* Always provide an absolute path to your scripts (i.e. `/usr/local/bin/update_check`)

* Don't invoke crontab with `sudo` (i.e. `sudo crontab -e`) unless you actually want the root user to run your script.

* There's no need to 'restart' cron after making changes with `crontab`. 




