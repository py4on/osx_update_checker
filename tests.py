#!/usr/bin/env python3

import unittest
from update_check import check_if_updates

class TestUpdateChecker(unittest.TestCase):
    
    def test_outputs_available_updates(self):
        updates_available = """\
        Software Update Tool
        Copyright 2002-2012 Apple Inc.
        
        Finding available software
        Software Update found the following new or updated software:
        * Command Line Tools (OS X 10.10)-6.4
        Command Line Tools (OS X 10.10) (6.4), 161762K [recommended]
        * OSXUpd10.10.4-10.10.4
        OS X Update (10.10.4), 1066078K [recommended] [restart]
        
        
        """
                
        result = check_if_updates(updates_available)
        self.assertEqual(result, '\n'.join(updates_available.split('\n')[4:]))

    def test_detects_no_updates_available(self):
        no_updates_available = """\
        Software Update Tool
        Copyright 2002-2015 Apple Inc.
        
        Finding available software
        
        """
        
        result = check_if_updates(no_updates_available)
        self.assertFalse(result)
        
        
        
if __name__ == "__main__":
    unittest.main()
